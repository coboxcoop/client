const fs = require('fs')
const path = require('path')
const os = require('os')
const debug = require('@coboxcoop/logger')('@coboxcoop/client')
const axios = require('axios')
const crypto = require('crypto')
const cookie = require('cookie')
const { pick, remap, hex, buildBaseURL } = require('./util')

const UNAUTHORIZED = 401

class Client {
  constructor (opts = {}) {
    this.endpoint = opts.endpoint || buildBaseURL({ hostname: 'localhost', port: '9112' })
    this.id = opts.id || hex(crypto.randomBytes(2))
    debug(`create client: endpoint ${this.endpoint}`)
  }

  async get (resource, params, opts) {
    return this._request({
      method: 'GET',
      url: this._url(resource),
      params,
      ...opts
    })
  }

  async post (resource, params, data, opts) {
    return this._request({
      method: 'POST',
      url: this._url(resource),
      params,
      data,
      ...opts
    })
  }

  async put (resource, params, data, opts) {
    return this._request({
      method: 'PUT',
      url: this._url(resource),
      params,
      data,
      ...opts
    })
  }

  async patch (resource, params, data, opts) {
    return this._request({
      method: 'PATCH',
      url: this._url(resource),
      params,
      data,
      ...opts
    })
  }

  async delete (resource, params, opts) {
    return this._request({
      method: 'DELETE',
      url: this._url(resource),
      params,
      ...opts
    })
  }

  _url (path) {
    if (Array.isArray(path)) path = path.join('/')
    else if (path[0] === '/') path = path.slice(1)
    return this.endpoint + '/' + path
  }

  async _request (opts) {
    const headers = {
      'Content-Type': 'application/json',
      ...this._getSessionHeaders(await this._loadSession()),
      ...opts.headers || {}
    }

    const axiosOpts = {
      method: opts.method || 'GET',
      url: opts.url || this._url(opts.path),
      maxRedirects: 0,
      timeout: 5000,
      headers: headers,
      data: opts.data || {},
      responseType: opts.responseType,
      params: opts.params
    }

    debug('request', axiosOpts)

    try {
      const response = await axios.request(axiosOpts)
      const setCookie = response.headers['set-cookie']
      // if we were sent a cookie with session id, save it to tmp directory
      if (setCookie && setCookie[0]) {
        const cookieOpts = cookie.parse(setCookie[0])
        const sid = cookieOpts['connect.sid']
        if (sid) await this._saveSession(cookieOpts['connect.sid'])
      }
      return response.data
    } catch (err) {
      if (err.response && err.response.status === UNAUTHORIZED) {
        await this._deleteSession()
      }
      debug(err)
      throw err
    }
  }

  async _loadSession () {
    const tmpdir = path.join(os.tmpdir(), 'cobox')
    try {
      await fs.promises.access(tmpdir)
    } catch (err) {
      await fs.promises.mkdir(tmpdir)
      return {}
    }

    try {
      const sid = await fs.promises.readFile(path.join(tmpdir, 'sid'), 'utf-8')
      return { sid: sid.trim() }
    } catch (err) {
      return {}
    }
  }

  async _saveSession (sid) {
    const tmpdir = path.join(os.tmpdir(), 'cobox')
    try {
      await fs.promises.access(tmpdir)
    } catch (err) {
      await fs.promises.mkdir(tmpdir)
    }

    try {
      await fs.promises.writeFile(path.join(tmpdir, 'sid'), sid)
    } catch (err) {
      console.error('failed to save sid to tmp directory', err)
    }
  }

  async _deleteSession () {
    const tmpdir = path.join(os.tmpdir(), 'cobox')
    try {
      await fs.promises.unlink(path.join(tmpdir, 'sid'))
    } catch (err) {
      debug(err)
    }
  }

  _getSessionHeaders (opts) {
    const headers = remap(pick(opts, ['sid']), {
      sid: 'connect.sid',
      session: 'connect.sid'
    })
    const cookieOpts = pick(headers, ['connect.sid'])
    const cookie = Object.entries(cookieOpts).reduce((acc, [key, value]) => {
      acc.push(`${key}=${value}`)
      return acc
    }, []).join(',')

    return {
      ...headers,
      cookie
    }
  }
}

module.exports = Client
