function buildBaseURL (opts) {
  return ['http://', opts.hostname || 'localhost', ':', opts.port, '/api'].join('')
}

function pick (obj, keys) {
  const result = {}
  for (const key of keys) {
    if (obj[key] !== undefined) result[key] = obj[key]
  }
  return result
}

function remap (args, keys) {
  return Object.keys(args).reduce((acc, key) => {
    const remapping = keys[key] || key
    acc[remapping] = args[key]
    return acc
  }, {})
}

function hex (b) {
  if (Buffer.isBuffer(b)) return b
  return b.toString('hex')
}

module.exports = {
  hex,
  pick,
  remap,
  buildBaseURL
}
